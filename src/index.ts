export async function render(): Promise<void> {
	// TODO render your app here
	const API_KEY = 'api_key=1aa0db4ff67fe3e65e983c1ab6053ab1',
		BASE_URL = 'https://api.themoviedb.org/3',
		API_URL = BASE_URL + '/discover/movie?sort_by=popularity.desc&' + API_KEY,
		IMG_URL = 'https://image.tmdb.org/t/p/w500',
		IMG_URL_BANNER = 'https://image.tmdb.org/t/p/original',
		searchURL = BASE_URL + '/search/movie?' + API_KEY,

		form = document.querySelector('.form-inline'),
		search = form?.querySelector('#search'),
		searchBtn = form?.querySelector('#submit'),
		films = document.getElementById('film-container'),
		divFilms = films?.querySelectorAll("div"),

		randomMovie = document.querySelector('#random-movie'),
		randomMovieName = document.querySelector('#random-movie-name'),
		randomMovieDesc = document.querySelector('#random-movie-description'),

		favoriteMovies = document.querySelectorAll('#favorite-movies'),
		svg = document.querySelectorAll(".bi-heart-fill"),
		
		loadMoreBtn = document.querySelector('#load-more');
		let currentPage = 1,
		lastUrl = '',
		nextPage = 2,
		totalPages = 100;
		
	getMovies(API_URL);

	function getMovies(url) {
		lastUrl = url;
		fetch(url).then(res => res.json()).then(data => {
			console.log(data.results)
			if (data.results.length !== 0) {
				showMovies(data.results);
				currentPage = data.page;
				nextPage = currentPage + 1;
				totalPages = data.total_pages;
				const setMovie = data.results[Math.floor(Math.random() * data.results.length - 1)];
				randomMovie.style.background = "url(" + IMG_URL_BANNER + setMovie.backdrop_path + ") no-repeat top center /cover";
				randomMovieName.innerText = setMovie.title;
				randomMovieDesc.innerText = setMovie.overview;
			}
	})
	}

	loadMoreBtn.addEventListener('click', () => {
	if (nextPage <= totalPages) {
		pageCall(nextPage);
		}
	})
	function pageCall(page) {
		let urlSplit = lastUrl.split('?');
		let queryParams = urlSplit[1].split('&');
		let key = queryParams[queryParams.length - 1].split('=');
		if (key[0] != 'page') {
			let url = lastUrl + '&page=' + page
			getMovies(url);
		} else {
			key[1] = page.toString();
			let a = key.join('=');
			queryParams[queryParams.length - 1] = a;
			let b = queryParams.join('&');
			let url = urlSplit[0] + '?' + b
			getMovies(url);
		}
	}
	
	function showMovies(data) {
		films.innerHTML = "";
		data.forEach(movie => {
			const { title, poster_path, vote_average, overview, release_date } = movie;
			const divFilms = document.createElement('div');
			divFilms.className = "col-lg-3 col-md-4 col-12 p-2";
		divFilms.innerHTML = `
						<div class="card shadow-sm">
							<img src="${poster_path ? IMG_URL + poster_path : "http://via.placeholder.com/1080x1580"}" alt="${title}" />
							<svg xmlns="http://www.w3.org/2000/svg" stroke="red" fill="#ff000078" width="50" height="50" class="bi bi-heart-fill position-absolute p-2" viewBox="0 -2 18 22">
								<path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" />
							</svg>
							<div class="card-body">
							<div class="movie-info d-flex">
                <h3>${title}</h3>
                <span class="${getColor(vote_average)}">${vote_average}</span>
            	</div>
								<p class="card-text truncate">
									${overview}
								</p>
								<div class="
                                            d-flex
                                            justify-content-between
                                            align-items-center
                                        ">
									<small class="text-muted">${release_date}</small>
								</div>
							</div>
						</div>
		`
			films.append(divFilms);
		})
		
	}

	function getColor(vote) {
		if (vote >= 8) {
			return 'green'
		} else if (vote >= 5) {
			return "orange"
		} else {
			return 'red'
		}
	}

	form.addEventListener('submit', (e) => {
		e.preventDefault();
		searchBtn.addEventListener("click", () => {
		const searchTerm = search.value;
		if (searchTerm) {
			getMovies(searchURL + '&query=' + searchTerm)
		} else {
			getMovies(API_URL);
			}
		})
	})
}
	